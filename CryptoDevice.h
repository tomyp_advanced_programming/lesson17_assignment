#pragma once
#include <cstdio>
#include <iostream>
#include <string.h>
#include <cstdlib>
#include "osrng.h"
#include "modes.h"
#include <iomanip>
#include "aes.h"
#include "filters.h"
#include "md5.h"
#include "hex.h"
#include "pch.h"
 #include "rsa.h"
#include "asn.h"
#include "oids.h"
#include "modarith.h"
#include "nbtheory.h"
#include "sha.h"
#include "algparam.h"
#include "fips140.h"
#include <integer.h>




class CryptoDevice
{

public:
    std::string encryptAES(std::string);
    std::string decryptAES(std::string);
	std::string encryptRSA(std::string);
	std::string decryptRSA(CryptoPP::Integer);


private:
    CryptoPP::byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];

};
