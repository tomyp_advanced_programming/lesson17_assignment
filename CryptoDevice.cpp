#include "CryptoDevice.h"

//aquire a default key for AES and block size for CBC. can't generate randomly, since all the agents shuold have the same key.
//can initialize with a unique sequence but not needed.

std::string CryptoDevice::encryptAES(std::string plainText)
{

    std::string cipherText;
	//
	// Dump Plain Text
	//
	std::cout << "Plain Text (" << plainText.size() << " bytes)" << std::endl;
	std::cout << plainText;
	std::cout << std::endl << std::endl;

	//
	// Create Cipher Text
	//
	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(cipherText));
	stfEncryptor.Put(reinterpret_cast<const unsigned char*>(plainText.c_str()), plainText.length() + 1);
	//stfEncryptor.Put(reinterpret_cast<const CryptoPP::byte*>(plainText.c_str()), plainText.length() + 1);
	stfEncryptor.MessageEnd();

	//
	// Dump Cipher Text
	//
	std::cout << "Cipher Text (" << cipherText.size() << " bytes)" << std::endl;

	for (int i = 0; i < cipherText.size(); i++)
	{

//		std::cout << "0x" << std::hex << (0xFF & static_cast<byte>(cipherText[i])) << " ";
	}

	std::cout << std::endl << std::endl;

	return cipherText;
}

std::string CryptoDevice::decryptAES(std::string cipherText)
{

    std::string decryptedText;
	//
	// Decrypt
	//
	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedText));
	stfDecryptor.Put(reinterpret_cast<const unsigned char*>(cipherText.c_str()), cipherText.size());
	stfDecryptor.MessageEnd();

	//
	// Dump Decrypted Text
	//
	std::cout << "Decrypted Text: " << std::endl;
	std::cout << decryptedText;
	std::cout << std::endl << std::endl;


	return decryptedText;
}
std::string CryptoDevice::encryptRSA(std::string plainText)
{

	CryptoPP::Integer n("0xbeaadb3d839f3b5f"), e("0x11"), d("0x21a5ae37b9959db9");
	CryptoPP::
	RSA::PublicKey pubKey;
	pubKey.Initialize(n, e);

	/////////////////////////////////////////////////////////
	
	CryptoPP::Integer c;
	std::string message = plainText;

	std::cout << "message: " << message << std::endl;

	// Treat the message as a big endian byte array
	CryptoPP::Integer m((const CryptoPP::byte *)message.data(), message.size());
	std::cout << "m: " << std::hex << m << std::endl;

	// Encrypt
	c = pubKey.ApplyFunction(m);
	std::cout << "c: " << std::hex << c << std::endl;
	std::stringstream stream;
	stream << std::hex << m;
	return stream.str();
}
std::string CryptoDevice::decryptRSA(CryptoPP::Integer cipherText)
{
	CryptoPP::Integer n("0xbeaadb3d839f3b5f"), e("0x11"), d("0x21a5ae37b9959db9");
	CryptoPP::AutoSeededRandomPool prng;

	CryptoPP::RSA::PrivateKey privKey;
	privKey.Initialize(n, e, d);

	/////////////////////////////////////////////////////////

	CryptoPP::Integer c(cipherText), r;
	std::string recovered;

	// Decrypt
	r = privKey.CalculateInverse(prng, c);
	std::cout << "r: " << std::hex << r << std::endl;

	// Round trip the message
	size_t req = r.MinEncodedSize();
	recovered.resize(req);
	r.Encode((CryptoPP::byte *)recovered.data(), recovered.size());

	std::cout << "recovered: " << recovered << std::endl;
	return recovered;
}